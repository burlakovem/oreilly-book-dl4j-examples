package ru.bdev.dl;

import ru.bdev.dl.iter.StockCsvRowsIter;

import java.io.IOException;

public class StockRNN {

    private static final String PATH_TO_FILE = "./currency/A.csv";

    public static void main(String[] args) throws IOException {
        StockCsvRowsIter iter = getData();
        System.out.println(iter.next());
        System.out.println(iter.next());
    }

    public static StockCsvRowsIter getData() throws IOException {
        return new StockCsvRowsIter(PATH_TO_FILE, 100, 30);
    }

}
