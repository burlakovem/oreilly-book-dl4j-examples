package ru.bdev.dl;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.helper.HttpConnection;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class CoiParser {

    private static final String BASE_URL = "https://www.gl5.ru/coi-viktor.html";

    public static void main(String[] args) throws URISyntaxException, InterruptedException, IOException {
        Document homePage = getHomePage();

        Element article = homePage.getElementsByTag("article").get(0);

        Elements links = article.getElementsByTag("a");

        for (Element a: links) {
            System.out.println(a.text());
        }

    }

    public static Document getHomePage() throws IOException {
        Connection.Response response = Jsoup.connect(BASE_URL).method(Connection.Method.GET).execute();
        return response.parse();
    }

//    public static Document getSongWords(String uri) throws IOException {
//        Connection.Response response = Jsoup.connect(uri).method(Connection.Method.GET).execute();
//
//        Document document = response.parse();
//    }

}
