package ru.bdev.dl.iter;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.factory.Nd4j;
import ru.bdev.dl.dto.StockDTO;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StockCsvRowsIter {

//    private List<StockDTO> dtos = new ArrayList<>();
//    private Stream<String> lines;

    private String pathToFile;
    private Integer batchSize;
    private Integer exampleLength;

    private Long count = 0L;
    private Integer currentBach = 0;

    public StockCsvRowsIter(String pathToFile, Integer batchSize, Integer exampleLength) throws IOException {
        this.pathToFile = pathToFile;
        this.batchSize = batchSize;
        this.exampleLength = exampleLength;

        this.count = getCsvFile(pathToFile).count();
    }

    private Stream<String> getCsvFile(String pathToFile) throws IOException {
        List<StockDTO> stockDTOS = new ArrayList<>();

        Path path = Paths.get(pathToFile);
        BufferedReader reader = Files.newBufferedReader(path);

        reader.readLine();

//        while (line != null) {
//            String[] row = line.split(",");
//            StockDTO dto = new StockDTO();
//
//            dto.setOpen(Double.parseDouble(row[2]));
//            dto.setClose(Double.parseDouble(row[3]));
//            dto.setHight(Double.parseDouble(row[4]));
//            dto.setLow(Double.parseDouble(row[5]));
//            dto.setAdjclose(Double.parseDouble(row[6]));
//
//            stockDTOS.add(dto);
//        }

        return reader.lines();
    }

    public DataSet next() throws IOException {
        long startIndex = this.currentBach * this.batchSize;
        long endIndex = startIndex + this.batchSize;

        endIndex = Math.min(endIndex, count);

        INDArray input = Nd4j.create('f');
        INDArray labels = Nd4j.create('f');

        for(long i=startIndex; i<endIndex; i++) {

            String line = getCsvFile(pathToFile).skip(i).limit(1).collect(Collectors.toList()).get(0);

            input.putRow(i, Nd4j.create(getRow(line), 'f'));

            long startIndex2 = startIndex+1;
            long endIndex2 = startIndex2+exampleLength;

            endIndex2 = Math.min(endIndex2, count);

            for(long j=startIndex2; j<endIndex2; j++) {
                String line2 = getCsvFile(pathToFile).skip(j).limit(1).collect(Collectors.toList()).get(0);
                labels.putRow(j, Nd4j.create(getRow(line2), 'f'));
            }

            if(i==2) break;

        }

        this.currentBach++;

        return new DataSet(input, labels);
    }

    public double[] getRow(String line) {
        String[] row = line.split(",");

        return new double[] {
            Double.parseDouble(row[2]),
            Double.parseDouble(row[3]),
            Double.parseDouble(row[4]),
            Double.parseDouble(row[5]),
            Double.parseDouble(row[6])
        };
    }

}
