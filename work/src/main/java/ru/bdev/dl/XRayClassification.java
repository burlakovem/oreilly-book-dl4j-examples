package ru.bdev.dl;

import org.datavec.api.io.labels.ParentPathLabelGenerator;
import org.datavec.api.split.FileSplit;
import org.datavec.image.loader.NativeImageLoader;
import org.datavec.image.recordreader.ImageRecordReader;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.ConvolutionLayer;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.conf.layers.SubsamplingLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;
import org.nd4j.linalg.learning.config.AMSGrad;
import org.nd4j.linalg.learning.config.AdaGrad;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Random;

public class XRayClassification {

    private static final Logger log = LoggerFactory.getLogger(XRayClassification.class);

    private static final int height = 180;
    private static final int width = 180;
    private static final int nChannels = 1;

    public static void main( String[] args ) throws Exception {
        int iterations = 2;

        RecordReaderDataSetIterator trainIterator = geTrainIterator();
        RecordReaderDataSetIterator testIterator = geTestIterator();

        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
            .l2(0.0005)
            /*
                Uncomment the following for learning decay and bias
             */
            //.learningRateDecayPolicy(LearningRatePolicy.Inverse).lrPolicyDecayRate(0.001).lrPolicyPower(0.75)
            .activation(Activation.RELU)
            .weightInit(WeightInit.XAVIER)
            .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
            .updater(new Nesterovs(0.5, 0.9))
            .list()
            .layer(0, new ConvolutionLayer.Builder(31, 31)
                .stride(3, 3)
                .nIn(nChannels)
                .nOut(64)
                .activation(Activation.RELU)
                .updater(new AMSGrad())
                .dropOut(0.3)
                .build()) // 28
            .layer(1, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
                .kernelSize(3, 3)
                .stride(1, 1)
                .build()) // 26
            .layer(2, new ConvolutionLayer.Builder(10, 10)
                .stride(1, 1)
                .nOut(128)
                .activation(Activation.RELU)
                .updater(new AMSGrad())
                .dropOut(0.3)
                .build()) // 17
            .layer(3, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
                .kernelSize(3, 3)
                .stride(1, 1)
                .build()) // 15
            .layer(4, new ConvolutionLayer.Builder(10, 10)
                .stride(1, 1)
                .nOut(256)
                .activation(Activation.RELU)
                .updater(new AMSGrad())
                .dropOut(0.3)
                .build()) // 5
            .layer(5, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
                .kernelSize(2, 2)
                .stride(1, 1)
                .build()) // 4
            .layer(6, new DenseLayer.Builder()
                .activation(Activation.RELU)
                .dropOut(0.3)
                .nOut(512)
                .build())
            .layer(7, new OutputLayer.Builder(LossFunctions.LossFunction.XENT)
                .nOut(trainIterator.getLabels().size())
                .activation(Activation.SIGMOID)
                .build())
            .setInputType(InputType.convolutionalFlat(height, width, nChannels)) //See note below
            .build();

        MultiLayerNetwork net = new MultiLayerNetwork(conf);
        net.init();
        net.setListeners(new ScoreIterationListener(100));

        for(int i=0; i<iterations; i++) {
            log.info("Iteration - " + i);
            trainIterator.reset();
            net.fit(trainIterator);
        }
        trainIterator.reset();

        Evaluation eval = new Evaluation(testIterator.getLabels().size());
        while(trainIterator.hasNext()){
            DataSet ds = trainIterator.next();
            INDArray currentLabel = ds.getLabels();
            INDArray output = net.output(ds.getFeatures(), false);
            System.out.println("===============");
            System.out.println("current - " + currentLabel);
            System.out.println("output  - " + output);
            System.out.println("===============");
            eval.eval(currentLabel, output);
        }
        log.info(eval.stats());
    }

    private static RecordReaderDataSetIterator geTrainIterator() throws URISyntaxException, IOException {
        return getIterator("./x-ray/train", 1, 1);
    }

    private static RecordReaderDataSetIterator geTestIterator() throws URISyntaxException, IOException {
        return getIterator("./x-ray/test", 1, 1);
    }

    private static URL getResource(String path) throws MalformedURLException {
//        return BirdsClassificationDL.class.getClassLoader().getResource(path);
        return new File(path).toURL();
    }

    private static RecordReaderDataSetIterator getIterator(String path, int batchSize, int labelIndex) throws URISyntaxException, IOException {
        File folder = new File(getResource(path).toURI());
        FileSplit split = new FileSplit(folder, NativeImageLoader.ALLOWED_FORMATS, new Random(123));

        ParentPathLabelGenerator parentPathLabelGenerator = new ParentPathLabelGenerator();

        ImageRecordReader recordReader = new ImageRecordReader(height, width, nChannels, parentPathLabelGenerator);
        recordReader.initialize(split);

        RecordReaderDataSetIterator iterator = new RecordReaderDataSetIterator(recordReader, batchSize, labelIndex, folder.listFiles().length);

        DataNormalization scaler = new ImagePreProcessingScaler(0, 1);

        scaler.fit(iterator);
        iterator.setPreProcessor(scaler);
//        iterator.setPreProcessor(new ImageFlatteningDataSetPreProcessor());

        return iterator;
    }

}
