package ru.bdev.dl;

import org.datavec.api.io.labels.ParentPathLabelGenerator;
import org.datavec.api.split.FileSplit;
import org.datavec.image.loader.NativeImageLoader;
import org.datavec.image.recordreader.ImageRecordReader;
import org.datavec.image.transform.ColorConversionTransform;
import org.datavec.image.transform.FlipImageTransform;
import org.datavec.image.transform.ImageTransform;
import org.datavec.image.transform.WarpImageTransform;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.ConvolutionLayer;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.conf.layers.SubsamplingLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;
import org.nd4j.linalg.learning.config.AMSGrad;
import org.nd4j.linalg.learning.config.AdaGrad;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.learning.config.RmsProp;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static org.bytedeco.opencv.global.opencv_imgproc.COLOR_BGR2YCrCb;

public class FruitClassificationDL {

    private static final Logger log = LoggerFactory.getLogger(FruitClassificationDL.class);

    private static final int height = 100;
    private static final int width = 100;
    private static final int nChannels = 3;

    public static void main( String[] args ) throws Exception {
        int iterations = 20;

        RecordReaderDataSetIterator trainIterator = getTrainIterator();
        RecordReaderDataSetIterator testIterator = getTestIterator();

        List<RecordReaderDataSetIterator> trainTranformIterators = new ArrayList<>();


//        ImageTransform flipTransform1 = new FlipImageTransform(new Random(seed));
//        ImageTransform flipTransform2 = new FlipImageTransform(new Random(123));
//        ImageTransform warpTransform = new WarpImageTransform(new Random(seed), 42);
//        ImageTransform colorTransform = new ColorConversionTransform(new Random(seed), COLOR_BGR2YCrCb);
//        List<ImageTransform> transforms = Arrays.asList(new ImageTransform[]{flipTransform1, warpTransform, flipTransform2, colorTransform});
//
//        for (ImageTransform transform : transforms) {
//            trainTranformIterators.add(getTrainTransformIterator(transform));
//        }

        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
//            .seed(seed)
            .l2(0.01)
            /*
                Uncomment the following for learning decay and bias
             */
            //.learningRateDecayPolicy(LearningRatePolicy.Inverse).lrPolicyDecayRate(0.001).lrPolicyPower(0.75)
            .activation(Activation.RELU)
            .weightInit(WeightInit.XAVIER)
            .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
            .updater(new AdaGrad(0.5))
            .list()
            .layer(0, new ConvolutionLayer.Builder(10, 10)
                .stride(3, 3)
                .nIn(nChannels)
                .nOut(10)
//                .activation(Activation.RELU)
//                .updater(new AMSGrad())
//                .dropOut(0.3)
                .build()) // 31
            .layer(1, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
                .kernelSize(3, 3)
                .stride(1, 1)
//                .dropOut(0.3)
                .build()) // 29
            .layer(2, new ConvolutionLayer.Builder(5, 5)
                .stride(2, 2)
                .nOut(100)
//                .activation(Activation.RELU)
//                .updater(new AMSGrad())
//                .dropOut(0.3)
                .build()) // 13
            .layer(3, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
                .kernelSize(3, 3)
                .stride(1, 1)
//                .dropOut(0.3)
                .build()) // 11
            .layer(4, new DenseLayer.Builder()
                .activation(Activation.RELU)
//                .dropOut(0.3)
                .nOut(500)
                .build())
            .layer(5, new OutputLayer.Builder(LossFunctions.LossFunction.MCXENT)
                .nOut(trainIterator.getLabels().size())
                .activation(Activation.SOFTMAX)
                .build())
            .setInputType(InputType.convolutionalFlat(height, width, nChannels)) //See note below
            .build();

//        conf.setIterationCount(iterations);

        MultiLayerNetwork net = new MultiLayerNetwork(conf);
        net.init();
        net.setListeners(new ScoreIterationListener(10));

        for(int i=0; i<iterations; i++) {
            log.info("Iteration - " + i);
            trainIterator.reset();
            net.fit(trainIterator);

            for (RecordReaderDataSetIterator iterator : trainTranformIterators) {
                iterator.reset();
                net.fit(iterator);
            }
        }

        trainIterator.reset();

        Evaluation eval = new Evaluation(testIterator.getLabels().size());
        while(testIterator.hasNext()){
            DataSet ds = testIterator.next();
            INDArray currentLabel = ds.getLabels();
            INDArray output = net.output(ds.getFeatures(), false);
            eval.eval(currentLabel, output);
        }
        log.info(eval.stats());

    }

    private static URL getResource(String path) throws MalformedURLException {
//        return BirdsClassificationDL.class.getClassLoader().getResource(path);
        return new File(path).toURL();
    }

    private static RecordReaderDataSetIterator getTrainIterator() throws URISyntaxException, IOException {
        return getIterator("./fruits/train", 1, 1);
    }

    private static RecordReaderDataSetIterator getTestIterator() throws URISyntaxException, IOException {
        return getIterator("./fruits/test", 1, 1);
    }

    private static RecordReaderDataSetIterator getIterator(String path, int batchSize, int labelIndex) throws URISyntaxException, IOException {
        return getIterator(path, batchSize, labelIndex, null);
    }

    private static RecordReaderDataSetIterator getTrainTransformIterator(ImageTransform imageTransform) throws URISyntaxException, IOException {
        return getIterator("./birds/train", 1, 1, imageTransform);
    }

    private static RecordReaderDataSetIterator getIterator(String path, int batchSize, int labelIndex, ImageTransform imageTransform) throws URISyntaxException, IOException {

        File folder = new File(getResource(path).toURI());
        FileSplit split = new FileSplit(folder, NativeImageLoader.ALLOWED_FORMATS, new Random(12345));

        ParentPathLabelGenerator parentPathLabelGenerator = new ParentPathLabelGenerator();

        ImageRecordReader recordReader = new ImageRecordReader(height, width, nChannels, parentPathLabelGenerator);

       if(imageTransform != null)
           recordReader.initialize(split, imageTransform);
       else
           recordReader.initialize(split);

        RecordReaderDataSetIterator iterator = new RecordReaderDataSetIterator(recordReader, batchSize, labelIndex, folder.listFiles().length);

        DataNormalization scaler = new ImagePreProcessingScaler(0, 1);

        scaler.fit(iterator);
        iterator.setPreProcessor(scaler);
//        iterator.setPreProcessor(new ImageFlatteningDataSetPreProcessor());

        return iterator;
    }
}
