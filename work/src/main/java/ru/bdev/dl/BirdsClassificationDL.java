package ru.bdev.dl;

import org.datavec.api.io.labels.ParentPathLabelGenerator;
import org.datavec.api.split.FileSplit;
import org.datavec.image.loader.NativeImageLoader;
import org.datavec.image.recordreader.ImageRecordReader;
import org.datavec.image.transform.ColorConversionTransform;
import org.datavec.image.transform.FlipImageTransform;
import org.datavec.image.transform.ImageTransform;
import org.datavec.image.transform.WarpImageTransform;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.datasets.iterator.MultipleEpochsIterator;
import org.deeplearning4j.datasets.iterator.impl.MnistDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.*;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.*;
import org.deeplearning4j.nn.conf.preprocessor.CnnToFeedForwardPreProcessor;
import org.deeplearning4j.nn.conf.preprocessor.FeedForwardToCnnPreProcessor;
import org.deeplearning4j.nn.conf.preprocessor.FeedForwardToRnnPreProcessor;
import org.deeplearning4j.nn.conf.preprocessor.RnnToCnnPreProcessor;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.ImageFlatteningDataSetPreProcessor;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;
import org.nd4j.linalg.learning.config.AMSGrad;
import org.nd4j.linalg.learning.config.AdaGrad;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.learning.config.RmsProp;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static org.bytedeco.opencv.global.opencv_imgproc.COLOR_BGR2YCrCb;

public class BirdsClassificationDL {

    private static final Logger log = LoggerFactory.getLogger(BirdsClassificationDL.class);

    private static final int height = 112;
    private static final int width = 112;
    private static final int nChannels = 3;

    public static void main( String[] args ) throws Exception {
        int seed = 43;
        int iterations = 30;

        RecordReaderDataSetIterator trainIterator = getTrainIterator();
        RecordReaderDataSetIterator testIterator = getTestIterator();

        List<RecordReaderDataSetIterator> trainTranformIterators = new ArrayList<>();


        ImageTransform flipTransform1 = new FlipImageTransform(new Random(seed));
        ImageTransform flipTransform2 = new FlipImageTransform(new Random(123));
        ImageTransform warpTransform = new WarpImageTransform(new Random(seed), 42);
        ImageTransform colorTransform = new ColorConversionTransform(new Random(seed), COLOR_BGR2YCrCb);
        List<ImageTransform> transforms = Arrays.asList(new ImageTransform[]{flipTransform1, warpTransform, flipTransform2, colorTransform});

        for (ImageTransform transform : transforms) {
            trainTranformIterators.add(getTrainTransformIterator(transform));
        }

        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
//            .seed(seed)
            .l2(0.005)
            /*
                Uncomment the following for learning decay and bias
             */
            //.learningRateDecayPolicy(LearningRatePolicy.Inverse).lrPolicyDecayRate(0.001).lrPolicyPower(0.75)
            .activation(Activation.RELU)
            .weightInit(WeightInit.XAVIER)
            .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
            .updater(new RmsProp(0.0001))
            .list()
            .layer(0, new ConvolutionLayer.Builder(5, 5)
                .stride(3, 3)
                .nIn(nChannels)
                .nOut(32)
//                .activation(Activation.RELU)
//                .updater(new AMSGrad())
//                .dropOut(0.3)
                .build()) // 35
            .layer(1, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
                .kernelSize(3, 3)
                .stride(1, 1)
                .build()) // 33
            .layer(2, new ConvolutionLayer.Builder(5, 5)
                .stride(1, 1)
                .nOut(64)
//                .activation(Activation.RELU)
//                .updater(new AMSGrad())
//                .dropOut(0.3)
                .build()) // 24
            .layer(3, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
                .kernelSize(3, 3)
                .stride(1, 1)
                .build()) // 22
            .layer(4, new ConvolutionLayer.Builder(5, 5)
                .stride(1, 1)
                .nOut(128)
//                .activation(Activation.RELU)
//                .updater(new AMSGrad())
//                .dropOut(0.3)
                .build()) // 13
            .layer(5, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
                .kernelSize(3, 3)
                .stride(1, 1)
                .build()) // 11
            .layer(6, new ConvolutionLayer.Builder(5, 5)
                .stride(1, 1)
                .nOut(256)
//                .activation(Activation.RELU)
//                .updater(new AMSGrad())
//                .dropOut(0.3)
                .build()) // 7
            .layer(7, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
                .kernelSize(3, 3)
                .stride(1, 1)
                .build()) // 5
            .layer(8, new ConvolutionLayer.Builder(3, 3)
                .stride(1, 1)
                .nOut(256)
//                .activation(Activation.RELU)
//                .updater(new AMSGrad())
//                .dropOut(0.3)
                .build()) // 3
            .layer(9, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
                .kernelSize(2, 2)
                .stride(1, 1)
                .build()) // 2
            .layer(10, new DenseLayer.Builder()
                .activation(Activation.RELU)
//                .dropOut(0.3)
                .nOut(512)
                .build())
            .layer(11, new OutputLayer.Builder(LossFunctions.LossFunction.MCXENT)
                .nOut(trainIterator.getLabels().size())
                .activation(Activation.SOFTMAX)
                .build())
            .setInputType(InputType.convolutionalFlat(height, width, nChannels)) //See note below
            .build();

//        conf.setIterationCount(iterations);

        MultiLayerNetwork net = new MultiLayerNetwork(conf);
        net.init();
        net.setListeners(new ScoreIterationListener(10));

        for(int i=0; i<iterations; i++) {
            log.info("Iteration - " + i);
            trainIterator.reset();
            net.fit(trainIterator);

            for (RecordReaderDataSetIterator iterator : trainTranformIterators) {
                iterator.reset();
                net.fit(iterator);
            }
        }

        trainIterator.reset();

        Evaluation eval = new Evaluation(testIterator.getLabels().size());
        while(testIterator.hasNext()){
            DataSet ds = testIterator.next();
            INDArray currentLabel = ds.getLabels();
            INDArray output = net.output(ds.getFeatures(), false);
            System.out.println("===============");
            System.out.println("current - " + currentLabel);
            System.out.println("output  - " + output);
            System.out.println("===============");
            eval.eval(currentLabel, output);
        }
        log.info(eval.stats());

    }

    private static URL getResource(String path) throws MalformedURLException {
//        return BirdsClassificationDL.class.getClassLoader().getResource(path);
        return new File(path).toURL();
    }

    private static RecordReaderDataSetIterator getTrainIterator() throws URISyntaxException, IOException {
        return getIterator("./birds/train", 10, 1);
    }

    private static RecordReaderDataSetIterator getTestIterator() throws URISyntaxException, IOException {
        return getIterator("./birds/test", 1, 1);
    }

    private static RecordReaderDataSetIterator getIterator(String path, int batchSize, int labelIndex) throws URISyntaxException, IOException {
        return getIterator(path, batchSize, labelIndex, null);
    }

    private static RecordReaderDataSetIterator getTrainTransformIterator(ImageTransform imageTransform) throws URISyntaxException, IOException {
        return getIterator("./birds/train", 10, 1, imageTransform);
    }

    private static RecordReaderDataSetIterator getIterator(String path, int batchSize, int labelIndex, ImageTransform imageTransform) throws URISyntaxException, IOException {

        File folder = new File(getResource(path).toURI());
        FileSplit split = new FileSplit(folder, NativeImageLoader.ALLOWED_FORMATS, new Random(12345));

        ParentPathLabelGenerator parentPathLabelGenerator = new ParentPathLabelGenerator();

        ImageRecordReader recordReader = new ImageRecordReader(height, width, nChannels, parentPathLabelGenerator);

       if(imageTransform != null)
           recordReader.initialize(split, imageTransform);
       else
           recordReader.initialize(split);

        RecordReaderDataSetIterator iterator = new RecordReaderDataSetIterator(recordReader, batchSize, labelIndex, folder.listFiles().length);

        DataNormalization scaler = new ImagePreProcessingScaler(0, 1);

        scaler.fit(iterator);
        iterator.setPreProcessor(scaler);
//        iterator.setPreProcessor(new ImageFlatteningDataSetPreProcessor());

        return iterator;
    }
}
